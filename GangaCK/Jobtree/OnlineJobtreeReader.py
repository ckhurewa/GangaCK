
import cPickle as pickle
from .. import GPI, logger, JobUtils, OfflineGPI, ConfigUtils
from . import ROOTDIR
from .BaseJobtreeReader import BaseJobtreeReader

#===============================================================================

# def compose_size(intjid, status):
#   # Bypass if not final
#   if status not in ('completed', 'failed'):
#     return '      N/A'
#   else:
#     size_local  = JobUtils.job_pfn_size(intjid)
#     _,size_lfn  = JobUtils.job_lfn_size(intjid)
#     if size_local is None:
#       size_local = 0
#     if size_lfn is None:
#       size_lfn = 0
#     return '{:>9}'.format(JobUtils.humansize(size_local+size_lfn)) 

# def compose_time(intjid, status):
#   pass

#===============================================================================

class OnlineJobtreeReader(BaseJobtreeReader):
  """
  Simple, based on existing GPI, but slow.
  """

  def ls(self, cwd=ROOTDIR):
    return GPI.jobtree.ls(cwd)

  @property
  def jobs_all(self):
    return GPI.jobs.ids()

  def job(self, jid):
    return GPI.jobs(jid)

#===============================================================================

#  3185 | completed |    Z02TauTau 8TeV |   60 |     Bender |      LSF |                    Z02TauTau (1kevt/f) [0:60] CheckP2PV

def load_pickle(jid):
  """
  Load the info from pickled index. Maybe inaccurate, but fast.
  """
  dname = ConfigUtils.dir_repository(jid)
  with open(dname+'.index') as fin:
    dat = pickle.load(fin)
  return dat[2]

def fastjob1(jid):
  logger.debug('loading: '+str(jid))
  dat         = load_pickle(jid)
  job         = JobUtils.JobStruct()
  job.fqid    = jid
  job.status  = dat['status']
  job.name    = dat['name']
  # job.lensj   = int(dat['display:subjobs'] or 0)
  job.application = app = dat['display:application']
  job.application_char = JobUtils._application_char[ app ]
  job.backend = dat['display:backend']
  job.comment = dat['display:comment']
  return job

def fastjob2(jid):
  """
  Alternaitve choice when index is not written yet, get from `data` file.
  """
  logger.debug('loading: '+str(jid))
  j0 = OfflineGPI.Job(jid)
  job         = JobUtils.JobStruct()
  job.fqid    = j0.fqid
  job.status  = j0.status
  job.name    = j0.name
  # job.lensj   = -1
  job.application = app = str(j0.application)
  job.application_char = JobUtils._application_char[ app ]
  job.backend = str(j0.backend)  
  job.comment = j0.comment
  return job


class QuickOnlineJobtreeReader(BaseJobtreeReader):
  """
  Like above, but rely on GPI.jobs table instead which is much faster, 
  but can be error-prone (due to setting in .gangarc) 
  and having limited information.

  TODO: Should pro-actively check def with config.Display.jobs_columns
  """

  bint    = r'\s*(\d*)\s*'
  bstr    = r'\s*(.*?)\s*' 
  pattern = r'^'+r'\|'.join([bint, bstr, bstr, bint, bstr, bstr, bstr])+r'$'

  # basedir = GPI.config.Configuration.gangadir
  # user    = GPI.config.Configuration.user

  def ls(self, cwd=ROOTDIR):
    # In some case, discrepancy of int/str jid. Strange.
    res = GPI.jobtree.ls(cwd)
    res['jobs'] = [int(jid) for jid in res['jobs']]
    return res

  @property
  def jobs_all(self):
    return GPI.jobs.ids()

  def job(self, jid):
    if int(jid) not in self.jobs_all:
      return None
    job = fastjob1(jid)
    # skip half-baked one.
    if job.status=='new' and job.name=='' and job.application=='Executable':
      job = fastjob2(jid)
    return job
