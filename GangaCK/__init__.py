
"""
Collection of helper methods to be unused inside Ganga6.

To bind with Ganga on start-up, add the following line to 
`~/.ipython-ganga/ipythonrc`

import_mod MyGanga

"""

__author__  = 'Chitsanu Khurewathanakul'
__email__   = 'chitsanu.khurewathanakul@epfl.ch'
__license__ = 'GNU GPLv3'

## 3rd-Party lib
from PythonCK import logger

## Flag
_HAS_GANGA = False

try:
  # Provide binders...
  from Ganga import GPI
  logger.reinit_blacklist() 
  logger.info("GangaCK imported successfully!")
  _HAS_GANGA  = True
except Exception as e:
  logger.exception(e)
  logger.warning("Cannot import library GangaCK, using OfflineGPI")
  import OfflineGPI as GPI

## Since Ganga 6.1
# Nullify the effect of decorator `register_line_magic` in the case where
# ipython context is not available (e.g., during pytest).
register_line_magic = lambda x: x
if _HAS_GANGA:
  try:
    from IPython import get_ipython
    if get_ipython():
      from IPython.core.magic import register_line_magic
  except ImportError as e: # no IPython available, probably outside GANGA env
    logger.warning(e)

## Finally, apply patching, as it still needs for both online/offline GPI.
import __patcher__
del __patcher__

# Ensure again log level after import
# logger.setLevel(logger.INFO)
