GangaCK package
===============

Subpackages
-----------

.. toctree::

    GangaCK.Jobtree
    GangaCK.OfflineGPI

Submodules
----------

GangaCK.AppUtils module
-----------------------

.. automodule:: GangaCK.AppUtils
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.ColorPrimer module
--------------------------

.. automodule:: GangaCK.ColorPrimer
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.Commands module
-----------------------

.. automodule:: GangaCK.Commands
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.ConfigUtils module
--------------------------

.. automodule:: GangaCK.ConfigUtils
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.Decorators module
-------------------------

.. automodule:: GangaCK.Decorators
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.IOUtils module
----------------------

.. automodule:: GangaCK.IOUtils
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.JobShell module
-----------------------

.. automodule:: GangaCK.JobShell
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.JobUtils module
-----------------------

.. automodule:: GangaCK.JobUtils
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.Magics module
---------------------

.. automodule:: GangaCK.Magics
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.TaskShell module
------------------------

.. automodule:: GangaCK.TaskShell
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.TaskUtils module
------------------------

.. automodule:: GangaCK.TaskUtils
    :members:
    :undoc-members:
    :show-inheritance:

GangaCK.Utils module
--------------------

.. automodule:: GangaCK.Utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: GangaCK
    :members:
    :undoc-members:
    :show-inheritance:
